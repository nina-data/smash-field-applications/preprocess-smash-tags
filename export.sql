create temporary view notes_parsed as
    select id,
           timezone('UTC'::text, to_timestamp(
               (ts / 1000)::double precision
             )) as creation_time,
           st_x(the_geom) as longitude,
           st_y(the_geom) as latitude,
           accuracy,
           text,
           form::jsonb as form
      from notes
;

create function pg_temp.smash_field(form jsonb, section text, name text, key text) returns text language plpgsql
as $$
begin
    return jsonb_path_query(
        form,
        '$[*]
            ? (@."sectionname" == $section)."forms"[*]
            ? (@."formname" == $name)."formitems"[*]
            ? (@."key" == $key)'
          ::jsonpath,
        jsonb_build_object(
            'section', section,
            'name', name,
            'key', key
          )
    ) ->> 'value';
end;
$$;

create procedure pg_temp.smash_export(section text, params text[][], year int) language plpgsql
as $$
declare
    fields text[] := ARRAY[
        'id', 'creation_time', 'longitude', 'latitude', 'accuracy', 'text'
      ];
    param text[];
    extracted text[];
begin
    foreach param slice 1 in array params
    loop
        extracted = array_append(extracted,
            format('pg_temp.smash_field(%I, %L, %L, %L) as %I',
                'form',
                section,
                param[1],
                param[2],
                param[1] || '_' || param[2]
              )
          );
    end loop;
    execute format(
            'create temporary view smash_export as '
            'select ' || array_to_string(fields || extracted, ', ') ||
            '  from notes_parsed'
            ' where text = %L and extract(year from creation_time) = cast(%L as int)'
            , section, year);
end;
$$;

call pg_temp.smash_export(:'form', :'fields', :'year');

select * from smash_export;