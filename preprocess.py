#!/usr/bin/env python3

import argparse
import json
import string
import urllib.request

import yaml


class EmptyNone(object):
    """Empty string"""

    def __nonzero__(self):
        return False

    def __str__(self):
        return ""

    def __getattr__(self, name):
        return EmptyNone

    def __getitem__(self, idx):
        return EmptyNone


class DropNone(string.Formatter):
    """Format None as an empty string"""

    def get_value(self, field_name, *args, **kwds):
        value = super().get_value(field_name, *args, **kwds)
        if value is None:
            return EmptyNone()
        return value


def fetch_json(url):
    """Shortcut to get a JSON file from an URL"""
    with urllib.request.urlopen(url) as fp:
        result = json.load(fp)
    return result


def fetch_datasette(url):
    """Fetch JSON data from Datasette with pagination"""
    while True:
        result = fetch_json(url)
        for row in result["rows"]:
            yield dict(zip(result["columns"], row))
        url = result.get("next_url")
        if not url:
            break


class Tags(object):
    """Pre-process custom GeoPaparazzi/SMASH tags.json contents"""

    def __init__(self, sections, fmt=DropNone()):
        self.sections = sections
        self.fmt = fmt

    def populate(self):
        return list(self.populate_selections(self.sections))

    def populate_selections(self, sections):
        for section in sections:
            if section["sectionname"] != "extras_common":
                yield dict(self.populate_section(section))

    def populate_section(self, section):
        for key, value in section.items():
            if key == "forms":
                yield (key, list(self.populate_forms(value)))
            else:
                yield (key, value)

    def populate_forms(self, forms):
        for form in forms:
            yield dict(self.populate_form(form))

    def populate_form(self, form):
        for key, value in form.items():
            if key == "formitems":
                yield (key, list(self.populate_formitems(value)))
            else:
                yield (key, value)

    def populate_formitems(self, formitems):
        for formitem in formitems:
            yield dict(self.populate_formitem(formitem))

    def populate_formitem(self, formitem):
        """Populate values using resources from values_extras"""
        values_extras = formitem.get("values_extras", [])
        items_values = set()
        for item in formitem.get("values", {}).get("items", []):
            items_values.add(item["item"])
        for values_extra in values_extras:
            if values_extra.get("source_type") == "datasette_json":
                for row in fetch_datasette(values_extra["source_url"]):
                    formatstring = values_extra["source_formatstring"]
                    value = self.fmt.format(formatstring, **row)
                    items_values.add(value)
        for key, value in formitem.items():
            if key == "values":
                yield (key, {"items": [{"item": value} for value in items_values]})
            elif key == "values_extras":
                pass
            else:
                yield (key, value)


def main(yaml_input, json_output):
    with open(yaml_input) as default:
        tags = yaml.safe_load(default)

    tags = Tags(tags)
    populated = tags.populate()
    with open(json_output, "w") as fp:
        json.dump(populated, fp)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Pre-process GeoPaparazzi/SMASH tags")
    parser.add_argument("yaml_input", help="YAML-equivalent of tags.json")
    parser.add_argument("json_output", help="Post-processed tags.json")
    args = parser.parse_args()
    main(args.yaml_input, args.json_output)
