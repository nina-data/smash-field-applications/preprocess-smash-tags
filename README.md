This software extends the functionality of GeoPaparazzi/SMASH tags.json file, thanks to:
1. Using YAML instead of JSON, which makes the file more compact (thanks to anchors which reduce code duplication) and easier to change (no need to worry about trailing commas)
2. Adding a `values_extra` section, to automatically populate `values` using external resources (Datasette JSON files are supported)

An `export.sql` parametric SQL file allows to export notes in plain CSV. Example:

```bash
psql -q --csv -f export.sql \
    -v "year=2024" \
    -v "form=artskartlegging" \
    -v "fields={{artsregistrering,art},{artsregistrering,egendefinert art},{artsregistrering,habitat},{artsregistrering,egendefinert habitat},{artsregistrering,substrate},{artsregistrering,egendefinert substrate},{artsregistrering,økologi},{artsregistrering,antall},{artsregistrering,observasjonstype},{artsregistrering,kartlegger},{kommentar/bilde,kommentar},{kommentar/bilde,bilde}}" \
    -o export.csv \
    $smash
```
